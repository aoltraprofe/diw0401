#README / LEEME

*Práctica:* 12

*Unidad:* UD04. Preprocesadores

*Módulo:* Diseño de Interfaces WEB

*Ciclo Formativo:* Desarrollo de Aplicaciones WEB

*Objetivo:* Uso de preprocesadores Sass. Se muestra también código .pug para el preprocesador de HTML.

##Install tools / Instalación herramientas

El uso de pug require de la instalación de *node.js* y, vía *npm*, de la instalación de *pug*.

   npm install pug

##Compile / Compilación

La compilación del fichero .pug se realiza usando el fichero *compila.js*

   node compila input.pug output.pug

##Author / Autor

Alfredo Oltra (mail: [alfredo.ptcf@gmail.com](alfredo.ptcf@gmail.com) / twitter: [@aoltra](https://twitter.com/aoltra))

##License / Licencia

Creative Commons: [by-no-sa](http://es.creativecommons.org/blog/licencias/) 

