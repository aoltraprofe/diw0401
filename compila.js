// Fichero Node.js para el preproceso de ficheros .pug -> HTML
// Fecha inicio: 22/12/2016
// Autor: Alfredo Oltra
// Licencia: GPLv3
// Uso: node compila input.pug output.html

var pug = require('pug');       // carga los módulos de pug
var fs = require('fs');         // carga al gestión del sistema de archivos

options = { pretty: true };

if (process.argv[2] == undefined || process.argv[3] == undefined) {
    return console.log("Número de parámetros inválido. Uso:\n\tnode compila input.pug output.html");
}

console.log("Compilando... " + process.argv[2] + " -> " + process.argv[3]);

const compiledFunction = pug.compileFile(process.argv[2],options);

fs.writeFile(process.argv[3], compiledFunction(), function(err) {
    if(err) {
        return console.log(err);
    }

    console.log("¡Fichero compilado con éxito!");
}); 